package components_abstractes;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class ComponentCompost extends ComponentNau {
	private Set<ComponentNau> components = new HashSet<ComponentNau>();

	public ComponentCompost(int capacitat, int pes) {
		super(pes);
		setCapacitat(capacitat);
	}

	@Override
	public boolean add(ComponentNau component) {
		boolean afegit = false;
		
		if (size() < getCapacitat())
			afegit = components.add(component);
		
		return afegit;
	}

	@Override
	public boolean remove(ComponentNau component) {
		return components.remove(component);
	}

	@Override
	public int size() {
		return components.size();
	}

	@Override
	public Set<ComponentNau> getComponents() {
		return Collections.unmodifiableSet(components);
	}
	
	@Override
	public String descriu() {
		return descriu(0);
	}
	
	protected String descriu(int sagnat) {
		String desc;
		int nEspais;
		String espais = "";
		
		for (nEspais=0; nEspais<sagnat; nEspais++)
			espais+="  ";
		
		desc = espais + toString() + " amb contingut:\n";
		
		for (ComponentNau component : components) {
			if (component instanceof ComponentCompost)
				desc += ((ComponentCompost)component).descriu(sagnat+1);
			else
				desc += espais+"  "+component.descriu();
		}
		desc+=espais+"Final contingut de "+getNom()+"\n";
		return desc;
	}

	@Override
	public int getPesTotal() {
		int pesTotal = getPes();
		
		for (ComponentNau component : components) {
			pesTotal += component.getPesTotal();
		}
		return pesTotal;
	}

	@Override
	public int getPotenciaTotal() {
		int potenciaTotal = getPotencia();
		
		for (ComponentNau component : components) {
			potenciaTotal += component.getPotenciaTotal();
		}
		return potenciaTotal;
	}
}

package domino;

import java.util.ArrayList;
import java.util.List;

/**
 * La classe Taula representa les fitxes que ja s'han
 * jugat. Com que totes les fitxes estan connectades,
 * una fitxa nova només es pot jugar a la dreta o a 
 * l'esquerra de totes les fitxes que ja hi ha.
 */
public class Taula {
	/**
	 * Llista que guarda totes les fitxes que s'han jugat.
	 */
	private List<Fitxa> fitxesJugades = new ArrayList<Fitxa>();
	/**
	 * @return la quantitat de fitxes que hi ha sobre la taula.
	 */
	public int mida() {
		return fitxesJugades.size();
	}
	/**
	 * Retorna el nombre que hi ha més a la dreta o més a l'esquerra
	 * de la taula. Aquests dos nombres són els únics amb els quals es
	 * pot enllaçar una nova fitxa.
	 * 
	 * @param pos  La posició que es vol mirar: DRETA o ESQUERRA.
	 * @return el número que queda a la dreta de la fitxa de més a la dreta,
	 * o el número que queda a l'esquerra de la fitxa de més a l'esquerra.
	 * @throws IllegalArgumentException si es passa CAP com a posició. 
	 */
	public int getNum(Posicio pos) throws IllegalArgumentException {
		int num;
		if (pos == Posicio.CAP)
			throw new IllegalArgumentException("La posició ha de ser dreta o esquerra");
		if (pos == Posicio.DRETA) {
			num = fitxesJugades.get(fitxesJugades.size()-1).get(Posicio.DRETA);
		} else {
			num = fitxesJugades.get(0).get(Posicio.ESQUERRA); 
		}
		return num;
	}
	/**
	 * Afegeix una nova fitxa a la taula, enllaçant-la a la dreta o
	 * a l'esquerra de les que ja hi ha. Si cal, es capgira la fitxa
	 * per tal que encaixi bé.
	 * 
	 * @param f  La fitxa que s'ha jugat.
	 * @param pos  DRETA o ESQUERRA.
	 * @throws IllegalArgumentException si es passa CAP com a posició,
	 * o si la fitxa no encaixa bé a la posició especificada.
	 */
	public void afegeix(Fitxa f, Posicio pos) throws IllegalArgumentException {
		Posicio posOnEncaixa;
		if (pos == Posicio.CAP)
			throw new IllegalArgumentException("La posició ha de ser dreta o esquerra");
		if (mida()!=0) {
			posOnEncaixa = f.encaixa(getNum(pos));
			if (posOnEncaixa == Posicio.CAP)
				throw new IllegalArgumentException("Aquesta fitxa no encaixa aquí");
			if (posOnEncaixa == pos)
				f.gira();
		}
		if (pos == Posicio.DRETA) {
			fitxesJugades.add(f);
		} else {
			fitxesJugades.add(0, f);
		}
	}
	/**
	 * Retorna una cadena amb una representació de les fitxes que
	 * ja s'han jugat.
	 * 
	 *  @return una cadena amb les fitxes que hi ha a la taula.
	 */
	@Override
	public String toString() {
		String s = "";
		for (Fitxa fitxa : fitxesJugades) {
			s+=fitxa.toString();
		}
		return s;
	}
}

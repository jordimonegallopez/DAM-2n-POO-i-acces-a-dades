package scrabble;

public class ScrabbleEx implements Comparable<ScrabbleEx> {
	private Casella[] caselles;
	
	public ScrabbleEx(Casella[] caselles) {
		this.caselles = caselles;
		
		// Si volguessim fer una còpia profunda seria així:
		/*
		this.caselles = new Casella[caselles.length];
		
		try {
			for (int i=0; i<caselles.length; i++)
				this.caselles[i] = (Casella) caselles[i].clone();
		} catch (CloneNotSupportedException e) {
			// No ha d'arribar aquí
			System.err.println(e);
		}*/
	}
	
	public int valor() {
		int multParaula = 1;
		int punts = 0;
		
		for (Casella c : caselles) {
			punts += Scrabble.valorDeLletra(c.getLletra()) * c.getMultiplicadorLletra();
			multParaula *= c.getMultiplicadorParaula();
		}
		punts *= multParaula;
		
		return punts;
	}

	@Override
	public int compareTo(ScrabbleEx arg0) {
		return valor() - arg0.valor();
	}
}

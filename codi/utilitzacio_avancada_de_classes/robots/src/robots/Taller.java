package robots;

public class Taller {
	private MarkJava[][] robots;
	
	public void torn() {
		for (int i=0; i<5;i++) {
			for (int j=0; j<5;j++) {
				if (robots[i][j] != null) {
					if (robots[i][j].decideixSiMou()) {
						resolMoviment(robots[i][j], i, j);
					}
				}
			}
		}
		for (int i=0; i<5; i++) {
			for (int j=0; j<5; j++) {
				if (robots[i][j] != null) {
					robots[i][j].haMogut = false;
				}
			}
		}
		mostraTaller();
	}
	
	public void resolMoviment(MarkJava robotAMoure, int x, int y) {
		int nouX = x, nouY = y;
		int aleatori = MarkJava.random.nextInt(4);
		switch (aleatori) {
			case 0: nouX = x+1; break;
			case 1: nouY = y+1; break;
			case 2: nouX = x-1; break;
			case 3: nouY = y-1; break;
		}
		if (nouX<0 || nouX>4 || nouY<0 || nouY>4)
			return;
		robotAMoure.gastaEnergia(1);
		if (robots[nouX][nouY] == null) {
			robots[nouX][nouY] = robotAMoure;
			robots[x][y] = null;
			if (robotAMoure instanceof Replicant) {
				creaUnNouRobot((Replicant)robotAMoure,x,y);
			}
		} else {
			robotAMoure.interactua(robots[nouX][nouY]);
		}
	}
	
	public void mostraTaller() {
		System.out.println();
		for (int i=0; i<5; i++) {
			for (int j=0; j<5; j++) {
				if (robots[i][j] == null)
					System.out.print("-");
				if (robots[i][j] instanceof Destructor)
					System.out.print("D");
				if (robots[i][j] instanceof Replicant)
					System.out.print("R");
				if (robots[i][j] instanceof Mecanic)
					System.out.print("M");
			}
			System.out.println();
		}
	}
	
	public void creaUnNouRobot(Replicant robotQueCrea, int x, int y) {
		robots[x][y] = robotQueCrea.construeix();
	}
	
	public static void main(String[] args) {
		Taller taller = new Taller();
		taller.creaTaller();
	}
	
	public void creaTaller() {
		robots = new MarkJava[5][5];
		robots[2][3] = new Replicant();
		robots[1][0] = new Destructor();
		robots[3][4] = new Mecanic();
		mostraTaller();
		for (int i=0; i<10; i++)
			torn();
	}
}

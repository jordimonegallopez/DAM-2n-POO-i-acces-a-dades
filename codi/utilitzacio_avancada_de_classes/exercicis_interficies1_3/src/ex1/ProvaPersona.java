package ex1;

public class ProvaPersona {
	public static void main(String[] args) {
		int comparacio;
		Persona p1 = new Persona();
		Persona p2 = new Persona(30, 65, 175);

		System.out.println("Primera persona: "+p1);
		System.out.println("Segona persona: "+p2);
		
		comparacio = p1.compareTo(p2);
		if (comparacio < 0)
			System.out.println("La primera és més baixa que la segona.");
		else if (comparacio > 0)
			System.out.println("La primera és més alta que la segona.");
		else // comparacio == 0
			System.out.println("Les dues persones medeixen igual.");
		
		comparacio = Persona.COMPARA_EDAT.compare(p1, p2);
		if (comparacio < 0)
			System.out.println("La primera és més jove que la segona.");
		else if (comparacio > 0)
			System.out.println("La primera és més vella que la segona.");
		else // comparacio == 0
			System.out.println("Les dues persones tenen la mateixa edat.");
		
		comparacio = Persona.COMPARA_PES.compare(p1, p2);
		if (comparacio < 0)
			System.out.println("La primera pesa menys que la segona.");
		else if (comparacio > 0)
			System.out.println("La primera pesa més que la segona.");
		else // comparacio == 0
			System.out.println("Les dues persones pesen el mateix.");
	}

}

package examenJDBC;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Film {
	public final int id;
	public final String title;
	public final int rentalDuration;
	public final double rentalRate;
	public final double replacementCost;
	
	public Film(int id, String title, int rentalDuration, double rentalRate, double replacementCost) {
		this.id = id;
		this.title = title;
		this.rentalDuration = rentalDuration;
		this.rentalRate = rentalRate;
		this.replacementCost = replacementCost;
	}
	
	public static Film byId(int id) throws SQLException {
		Film film = null;
		try (
			PreparedStatement st = DB.connection().prepareStatement("select film_id, title, rental_duration, rental_rate, replacement_cost from film where film_id = ?");
		) {
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				film = new Film(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getDouble(4), rs.getDouble(5));
			}
			rs.close();
			return film;
		}
	}
}

package metadades;

import java.sql.*;
import java.util.Scanner;

/** 2. Fes un programa que permeti a l'usuari escriure una sentència SELECT. 
 * Aquesta sentència s'executarà contra el servidor MySQL i el seu resultat 
 * (o l'error que es produeixi) es mostrarà per pantalla. Per mostrar els resultats, 
 * cal mostrar una capçalera amb el nom de cada columna retornada.
 */
public class Ex2 {
	public static void main(String[] args) {
		new Ex2().run();
	}
	
	public void run() {
		String sql = "";
		ResultSetMetaData rsmd;
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Introdueix la sentencia SQL");
		System.out.print("  >");
		
		while( !(sql += scanner.nextLine()).contains(";") ){
			sql += " ";
			System.out.print("...");
		}
		
		try (Connection connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/sakila", "root", "super3");
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery(sql);){
			rsmd = rs.getMetaData();
			int nColums = rsmd.getColumnCount();
			
			for(int i=1; i<=nColums; i++){
				System.out.print(rsmd.getColumnName(i) + ", ");
			}
			System.out.println();
			while(rs.next()) {
				for(int i=1; i<=nColums; i++){
					System.out.print(rs.getString(i)+", ");
				}
				System.out.println();
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}

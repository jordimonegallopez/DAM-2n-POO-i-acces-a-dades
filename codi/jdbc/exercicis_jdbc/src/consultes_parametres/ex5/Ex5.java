package consultes_parametres.ex5;

import java.util.List;

/**
 * 5- Fes un programa que permeti als usuaris consultar les pel·lícules disponibles en una botiga.
 * Primer, es mostraran totes les botigues, amb la seva adreça.
 * Un cop seleccionada una botiga, l'usuari podrà realitzar cerques pel nom de les pel·lícules, 
 * pel nom dels actors o pel gènere de les pel·lícules.
 * El resultat d'aquestes cerques mostrarà el títol de les pel·lícules d'aquesta botiga 
 * que compleixin els requisits, i si estan disponibles, o si no ho estan, la seva data de retorn.
 * A través de la llista de pel·lícules, se'n pot seleccionar una, i se'n mostraran els detalls: 
 * títol, descripció, any de producció, nacionalitat i actors que hi apareixen.
 * Separa la interacció amb l'usuari de la gestió de la base de dades, de manera
 * que aquest codi sigui reaprofitable.
 */
public class Ex5 {
	private DBAccess db = new DBAccess();
	private UserView view = new UserView();
	
	public static void main(String[] args) {
		(new Ex5()).run();
	}

	public void run() {
		int storeId = view.chooseStore(db.getStores());
		if (storeId>=0) {
			SearchCriteria criteria = view.menu();
			String searchString = view.askForString(criteria.info);
			List<Inventory> items = db.listMovies(criteria, searchString, storeId);
			view.showInventory(items);
			if (!items.isEmpty()) {
				Film film = db.getFilmByInventoryId(view.chooseItem(items));
				if (film != null)
					view.showFilm(film);
			}
		}
	}
}

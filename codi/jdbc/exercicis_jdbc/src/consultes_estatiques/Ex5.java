package consultes_estatiques;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * 5. Fes un programa que localitzi els clients que tenen pel·lícules 
 * que ja haurien d'haver retornat. Volem el nom i cognom i el telèfon 
 * dels clients i el títol de la pel·lícula que deuen.
 */
public class Ex5 {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/sakila";
		String user = "root";
		String passwd = "super";
		// consulta que retorna totes les pel·lícules llogades
		/* "SELECT name, phone, title FROM customer_list"+
			" JOIN rental ON customer_id=ID"+
			" JOIN inventory USING(inventory_id)"+
			" JOIN film USING(film_id)"+
			" WHERE return_date is null ORDER BY name;");*/
		// consulta que mostra només aquelles que ja s'haurien d'haver retornat
		String sql = "SELECT name, phone, title FROM customer_list"+
			" JOIN rental ON customer_id=ID"+
			" JOIN inventory USING(inventory_id)"+
			" JOIN film USING(film_id)"+
			" WHERE return_date is null" +
			" AND adddate(rental_date, rental_duration) < curdate()"+
			" ORDER BY name;";
		try (Connection connexio = DriverManager.getConnection(url, user, passwd);
				Statement statement = connexio.createStatement();
				ResultSet rs = statement.executeQuery(sql);) {			
			String nomCustomer;
			String nomCustomerAnterior = "";
			while (rs.next()){
				nomCustomer = rs.getString(1);
				System.out.print((nomCustomer.equals(nomCustomerAnterior)?"":
					"\n\nName: "+nomCustomer+"\n"+"\n Phone: "+rs.getString(2)+"\n")
					+" Title of pending film: "+rs.getString(3)+"\n");
				nomCustomerAnterior = nomCustomer;
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}

Materials per als mòduls *M3 - Programació orientada a objectes*, *M5 - Entorns de desenvolupament (disseny orientat a objectes)*, i *M6 - Accés a dades* del cicle de *Desenvolupament d'Aplicacions Multiplataforma*.
====

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Materials per als mòduls M3 - Programació orientada a objectes, M5 - Entorns de desenvolupament (disseny orientat a objectes), i M6 - Accés a dades del cicle de Desenvolupament d'Aplicacions Multiplataforma</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades" property="cc:attributionName" rel="cc:attributionURL">https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Índex
-----

[Configuració de l'entorn de desenvolupament](entorn_desenvolupament/readme.adoc)

### M3UF4: programació orientada a objectes (POO). Fonaments.

1. [Utilització avançada de classes](M3UF4/1-classes)

### M3UF5: POO. Llibreries de classes fonamentals.

1. Estructures d'emmagatzematge
  - [Teoria](M3UF5/1-estructures_emmagatzematge/readme.adoc)

  Activitats:

  - [Exercicis de piles i cues](M3UF5/1-estructures_emmagatzematge/piles_i_cues.md)
  - [Exercicis de llistes](M3UF5/1-estructures_emmagatzematge/llistes.md)
  - [Exercicis de conjunts i diccionaris](M3UF5/1-estructures_emmagatzematge/conjunts_i_diccionaris.md)
  - [Exercici del domino](M3UF5/1-estructures_emmagatzematge/domino.md)
  - [Exercici de la fàbrica de naus](M3UF5/1-estructures_emmagatzematge/fabrica_naus.md)
  - [Exercici de la pizzera d'en Luigi](M3UF5/1-estructures_emmagatzematge/pizzeria.md)

2. [Interfícies gràfiques](M3UF5/2-interficies_grafiques/README.md)

### M3UF6: POO. Introducció a la persistència en BD.

1. Accés a bases de dades amb JDBC
  - [Teoria](M3UF6/1-jdbc/readme.adoc)
  - [Exercicis](M3UF6/1-jdbc/exercicis.md)

### M5UF3: introducció al disseny orientat a objectes.

1. Diagrames UML
  - [Teoria](M5UF3/readme.adoc)
  - [Exercicis](M5UF3/exercicis.adoc)

### M6UF1: persistència en fitxers.

1. [Persistència en fitxers](M6UF1/1-fitxers/README.md)

### M6UF2: persistència en BDR-BDOR-BDOO.

### M6UF3: persistència en BD natives XML.

1. [Persistència en BD orientades a documents](M6UF3/1-mongodb/README.md)

### M6UF4: components d’accés a dades.

1. [Creació d'aplicacions web amb un framework](M6UF4/1-laravel/README.md)
